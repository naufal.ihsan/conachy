from algorithm.graph import Graph
from algorithm.city import City
from algorithm.astar import *

import json


def init_map(start, goal):
    """
        Initialize Map Graph.

        generate city from json to City Object and adding it to Graph

        Parameters
        ----------
        start : String
            start node
        goal : String
            goal node

        Returns
        -------
        Dict

            Information about map

    """

    g = Graph()

    with open("data/city.json") as f:
        java = json.load(f)

        for city in java:
            lat = float(java[city][0]['latitude'])
            lng = float(java[city][1]['longitude'])
            new_city = City(city, lat, lng)
            g.add_city(new_city)

        cities = g.nodes

        for city in cities:
            for i in range(2, len(java[city])):
                for destination, distance in java[city][i].items():
                    g.edge(cities[city], cities[destination], float(distance))

    start = cities[start]
    goal = cities[goal]

    path = a_star(start, goal).split(",")
    route = ""
    coordinate = list()

    # print(cities)

    for city in reversed(path):
        if city != '':
            coordinate.append({'latLng': {'lat': cities[city].lat, 'lng': cities[city].lng}})
            route += (city + "->")

    result = dict()
    result['path_coord'] = coordinate
    result['path'] = route
    result['start_lat'] = start.lat
    result['start_lng'] = start.lng
    result['goal_lat'] = goal.lat
    result['goal_lng'] = goal.lng

    return result
