from django.contrib import admin
from .models import Kota, TempatWisata, OlehOleh

# Register your models here.
admin.site.register(Kota)
admin.site.register(TempatWisata)
admin.site.register(OlehOleh)
