from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import TempatWisata, Kota, OlehOleh

from maps import init_map

response = {}

# Create your views here.
def index(request):
    return render(request, 'index.html')


def search(request):
    return render(request, 'search.html')


def destination(request , city , tourism):
    response['tourism'] = TempatWisata.objects.filter(kota=Kota.objects.get(nama=city)).filter(nama=tourism)[0]
    response['merchandise'] = OlehOleh.objects.filter(kota=Kota.objects.get(nama=city))[0]
    print(response)
    return render(request, 'destination.html' , response)


@csrf_exempt
def searching(request):
    if request.POST:
        start = request.POST['start'].lower()
        goal = request.POST['goal'].lower()
        tourism = request.POST['tourism'].strip().lower()
        result = init_map(start, goal)
        data = {
            'path_coord': result['path_coord'],
            'path': str(result['path']),
            'start_lat': result['start_lat'],
            'start_lng': result['start_lng'],
            'goal_lat': result['goal_lat'],
            'goal_lng': result['goal_lng'],
            'dest_city' : goal,
            'tourism' : tourism
        }

        return JsonResponse(data)

    return JsonResponse({'response': 'failed'})
