from django.db import models


# Create your models here.
class Kota(models.Model):
    kode = models.IntegerField()
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

    class Meta:
        ordering = ['kode']
        verbose_name = "Kota"
        verbose_name_plural = "Kota-Kota"


class TempatWisata(models.Model):
    kota = models.ForeignKey(Kota, on_delete=models.CASCADE)
    nama = models.CharField(max_length=255)
    urlgambar = models.CharField(max_length=255)
    deskripsi = models.TextField()
    harga = models.CharField(max_length=100)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = "Tempat Wisata"
        verbose_name_plural = "Tempat-Tempat Wisata"


class OlehOleh(models.Model):
    kota = models.ForeignKey(Kota, on_delete=models.CASCADE)
    nama = models.CharField(max_length=255)
    urlgambar = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name = "Oleh Oleh"
        verbose_name_plural = "Oleh Oleh"
