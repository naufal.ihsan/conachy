class City:
    def __init__(self, name, lat, lng):
        self.name = name
        self.lat = lat
        self.lng = lng
        self.priority = 0
        self.edges = list()
        self.cost = dict()
        self.path = list()

    def append(self, current):
        self.path.append(current)

    def __lt__(self, other):
        return self.priority < other.priority
