class Graph:
    def __init__(self):
        self.nodes = dict()

    def add_city(self, city):
        self.nodes[city.name] = city

    def edge(self, city1, city2, distance):
        self.nodes[city1.name].edges.append(city2)
        self.nodes[city2.name].edges.append(city1)
        self.nodes[city2.name].cost[city1.name] = self.nodes[city1.name].cost[city2.name] = distance

    def neighbors(self, name):
        return self.nodes[name].edges

    def cost(self, city1, city2):
        return self.nodes[city1.name].cost[city2.name]
