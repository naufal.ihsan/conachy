from collections import defaultdict
from .haversine import haversine
import sys


def a_star(start, goal, optimal_cost=False):
    """
        Finding shortest path cost.

        find a path to the given goal node having the smallest cost

        Parameters
        ----------
        start : City
            object City for start node
        goal : City
            object City for goal node
        optimal_cost : Boolean

        Returns
        -------
        if optimal_cost(True) return value float optimal distance
        else return Value String path from start to goal node

    """

    closed_set = set()
    open_set = {start}
    came_from = dict()
    g_score = defaultdict(lambda: float('inf'))
    g_score[start] = 0
    h_score = defaultdict(lambda: float('inf'))
    h_score[start] = 0
    f_score = defaultdict(lambda: float('inf'))
    f_score[start] = haversine((start.lat, start.lng), (goal.lat, goal.lng))

    while len(open_set):
        min_fs = sys.maxsize

        current = None

        for node in open_set:
            if f_score[node] < min_fs:
                min_fs = f_score[node]
                current = node

        if current == goal:
            all = reconstruct_path(came_from, current)

            if optimal_cost:
                result = 0
                for city in all.path:
                    result += g_score[city]
                return result

            result = goal.name + ","
            for city in all.path:
                result += (city.name + ",")

            admissible = admissible_heuristic(h_score, goal)

            if admissible:
                return result
            else:
                return None

        open_set.remove(current)
        closed_set.add(current)

        for neighbor in current.edges:
            if neighbor in closed_set:
                continue

            tentative_gscore = g_score[current] + dist_between(current, neighbor)

            if neighbor not in open_set:
                open_set.add(neighbor)
            elif tentative_gscore >= g_score[neighbor]:
                continue

            came_from[neighbor] = current
            g_score[neighbor] = tentative_gscore
            h_score[neighbor] = haversine((neighbor.lat, neighbor.lng), (goal.lat, goal.lng))
            f_score[neighbor] = g_score[neighbor] + h_score[neighbor]


def reconstruct_path(came_from, current):
    """
        Reconstruct path with the shortest path.

        Parameters
        ----------
        came_from: Dict
            Dictionary each node come from
        current : City

        Returns
        -------
        List
            All Path

    """
    total_path = current
    while current in came_from.keys():
        current = came_from[current]
        total_path.append(current)
    return total_path


def dist_between(current, neighbor):
    """
           Distance between City and its Neighbor

           Parameters
           ----------
           current: City

           neighbor : City

           Returns
           -------
           float

    """
    return current.cost[neighbor.name]


def admissible_heuristic(h_score, goal):
    """
        Check estimated cost must always be lower than or equal to the actual cost of reaching the goal state.

        Parameters
        ----------
        h_score: Dict
            Dict of heuristic of each node
        goal : City

        Returns
        -------
        Boolean

            if (True) maps is admissible
    """
    for key in h_score.keys():
        if h_score[key] > a_star(key, goal, optimal_cost=True):
            return False
    return True
