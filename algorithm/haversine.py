from math import radians, cos, sin, asin, sqrt

AVG_EARTH_RADIUS_KM = 6371.0088
AVG_EARTH_RADIUS_MI = 3958.7613
AVG_EARTH_RADIUS_NMI = 3440.0695


def haversine(point1, point2, miles=False, nautical_miles=False):
    """
        Calculate the great-circle distance between two points on the Earth surface.

        :input: two 2-tuples, containing the latitude and longitude of each point
        in decimal degrees.

        Keyword arguments:
        unit -- a string containing the initials of a unit of measurement (i.e. miles = mi)
                default 'km' (kilometers).

        Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))

        :output: Returns the distance between the two points.

        The default returned unit is kilometers.

    """

    if miles:
        avg_earth_radius = AVG_EARTH_RADIUS_MI
    elif nautical_miles:
        avg_earth_radius = AVG_EARTH_RADIUS_NMI
    else:
        avg_earth_radius = AVG_EARTH_RADIUS_KM

    lat1, lng1 = point1
    lat2, lng2 = point2

    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2

    return 2 * avg_earth_radius * asin(sqrt(d))
